package com.addokta.module;

import com.addokta.module.okta.OktaNativeModule;
import com.addokta.module.okta.OktaViewManager;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Pavel Martynov on 16.11.2017.
 */

public class AppReactPackage implements ReactPackage {

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.<ViewManager>singletonList(
                new OktaViewManager()
        );
    }

    @Override
    public List<com.facebook.react.bridge.NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<com.facebook.react.bridge.NativeModule> modules = new ArrayList<>();

        modules.add(new OktaNativeModule(reactContext));

        return modules;
    }
}
