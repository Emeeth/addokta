package com.addokta.module.okta;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.addokta.AuthorizedActivity;
import com.addokta.EmptyActivity;
import com.addokta.MainActivity;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import com.okta.appauth.android.OktaAppAuth;
import net.openid.appauth.AuthorizationException;

/**
 * Created by Pavel Martynov on 16.11.2017.
 */

public class OktaNativeModule extends ReactContextBaseJavaModule {

    public static final String REACT_CLASS = "OktaNativeModule";
    private static final String EXTRA_FAILED = "failed";

    public OktaNativeModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @ReactMethod
    public void isUserLoggedIn(final Callback successCallback, final Callback tokenFailureCallback){
        if(OktaAuth.isUserLoggedIn()){
            successCallback.invoke();
        }else{
            tokenFailureCallback.invoke();
        }
    }

    @ReactMethod
    public void init(final Callback successCallback, final Callback tokenFailureCallback) {
        Activity currentActivity = getCurrentActivity();
        OktaAuth.getInstance(currentActivity);

        // Do any of your own setup of the Activity

        OktaAuth.init(
                currentActivity,
                new OktaAppAuth.OktaAuthListener() {
                    @Override
                    public void onSuccess() {
                        // Handle a successful initialization (e.g. display login button)
                        successCallback.invoke();
                    }

                    @Override
                    public void onTokenFailure(@NonNull AuthorizationException ex) {
                        // Handle a failed initialization
                        tokenFailureCallback.invoke(ex.toString());
                    }
                });
    }

    @ReactMethod
    public void login() {
        Activity currentActivity = getCurrentActivity();
        Intent completionIntent = new Intent(currentActivity, MainActivity.class);
        Intent cancelIntent = new Intent(currentActivity, MainActivity.class);
        cancelIntent.putExtra(EXTRA_FAILED, true);
        cancelIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        OktaAuth.login(
                currentActivity,
                PendingIntent.getActivity(currentActivity, 0, completionIntent, 0),
                PendingIntent.getActivity(currentActivity, 0, cancelIntent, 0)
        );
    }

    @ReactMethod
    public void logout(final Callback callback) {
        OktaAuth.logout();
        callback.invoke();
    }
}
