package com.addokta.module.okta;

import android.app.PendingIntent;
import android.content.Context;

import com.okta.appauth.android.OktaAppAuth;

/**
 * Created by pavelmartynov on 23.11.2017.
 */

public class OktaAuth {

    private static volatile OktaAppAuth mOktaAuth;

    private OktaAuth(){

    }

    public static OktaAppAuth getInstance(Context context){
        if(mOktaAuth == null){
            synchronized (OktaAuth.class){
                if(mOktaAuth == null){
                    mOktaAuth = OktaAppAuth.getInstance(context);
                }
            }
        }
        return mOktaAuth;
    }

    public static OktaAppAuth getData(){
        return mOktaAuth;
    }

    public static boolean isUserLoggedIn(){
        return mOktaAuth.isUserLoggedIn();
    }

    public static void init(Context context, OktaAppAuth.OktaAuthListener listener){
        mOktaAuth.init(context, listener);
    }

    public static void login(final Context context, final PendingIntent completionIntent, final PendingIntent cancelIntent){
        mOktaAuth.login(context, completionIntent, cancelIntent);
    }

    public static void logout(){
        mOktaAuth.logout();
    }

    public static void hasIdToken(){
        mOktaAuth.hasIdToken();
    }

    public static void hasAccessToken(){
        mOktaAuth.hasAccessToken();
    }

    public static void getAccessTokenExpirationTime(){
        mOktaAuth.getAccessTokenExpirationTime();
    }
}
