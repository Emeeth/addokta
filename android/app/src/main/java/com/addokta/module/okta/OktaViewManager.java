package com.addokta.module.okta;

import android.widget.EditText;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.okta.appauth.android.OktaAppAuth.LoginHintChangeHandler;

import javax.annotation.Nullable;

/**
 * Created by pavelmartynov on 23.11.2017.
 */

public class OktaViewManager extends SimpleViewManager<EditText> {

    public static final String REACT_CLASS = "OktaViewManager";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected EditText createViewInstance(ThemedReactContext reactContext) {
        EditText editText = new EditText(reactContext);
        editText.addTextChangedListener(new LoginHintChangeHandler(OktaAuth.getData()));
        return editText;
    }

    @ReactProp(name = "hint")
    public void setHint(EditText view, @Nullable String hint) {
        view.setHint(hint);
    }
}
