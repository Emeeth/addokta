'use strict';
import PropTypes from 'prop-types';
import { NativeModules, requireNativeComponent, View } from 'react-native';

var iface = {
  name: 'OktaViewManager',
  propTypes: {
    hint: PropTypes.string,
    ...View.propTypes // include the default view properties
  },
};
module.exports = requireNativeComponent('OktaViewManager', iface);