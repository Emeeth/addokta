
'use strict';
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import OktaNative from '../module/OktaNativeModule';

export default class Preloader extends Component {
  _isUserLoggedIn() {
    OktaNative.isUserLoggedIn(
      () => {
        Actions.userModule({ type: ActionConst.RESET });
      },
      () => {
        Actions.loginModule({ type: ActionConst.RESET });
      })
  }

  componentDidMount() {
    OktaNative.init(() => {
      this._isUserLoggedIn();
    }, (msg) => {
      alert(msg);
    })
  }

  render() {
    return (
      <View>
        <Text style={styles.welcome}>Initializing...</Text>
        <ActivityIndicator style={{ flex: 1, marginTop: 10 }} color="#841584" size='large' />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    marginTop: 40,
    marginBottom: 20,
  }
});
