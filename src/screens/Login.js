
'use strict';
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Dimensions,
  Button
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import OktaNative from '../module/OktaNativeModule';
import OktaView from '../module/OktaViewManager';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      status: "Initializing..."
    }
  }

  componentDidMount() {
    OktaNative.isUserLoggedIn(
      () => {
        Actions.userModule({ type: ActionConst.RESET });
      }, () => {
        this.setState({ isVisible: false })
      })
  }

  render() {
    if (this.state.isVisible) {
      return (
        <View>
          <Text style={styles.welcome}>{this.state.status}</Text>
          <ActivityIndicator style={{ flex: 1, marginTop: 10 }} color="#841584" size='large' />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>OktaAuth Demo</Text>
        <View style={{ alignItems: 'center', marginBottom: 20 }}>
          <Button
            onPress={() => {
              // this.setState({ status: "Authorizing...", isVisible: true })
              OktaNative.login()
            }}
            title="Start authorization"
            color="#841584"
            accessibilityLabel="Tost"
          />
        </View>
        <Text style={{ color: 'black', marginTop: 10, marginBottom: 10, fontSize: 16 }}>Authorization options:</Text>
        <OktaView
          style={styles.oktaView}
          hint="Username (e.g. test@example.com)" />
        <Text style={styles.instructions}>The Username is optional. If specified, it is transmitted as a login_hint parameter in the authorization request.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    padding: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    marginTop: 40,
    marginBottom: 20,
  },
  instructions: {
    color: 'grey',
    marginBottom: 10,
  },
  oktaView: {
    height: 50,
    width: Dimensions.get('window').width - 2 * 20,
  },
});
