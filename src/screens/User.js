
'use strict';
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import OktaNative from '../module/OktaNativeModule';

export default class User extends Component {
  _login() {
    OktaNative.isUserLoggedIn(() => { },
      () => {
        Actions.loginModule({ type: ActionConst.RESET });
      })
  }

  componentDidMount() {
    this._login();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>User Screen</Text>
        <Button
          onPress={() => {
            // this.setState({ status: "Authorizing...", isVisible: true })
            OktaNative.logout(() => { this._login() });
          }}
          title="logout"
          color="#841584"
          accessibilityLabel="Tost"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    padding: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    marginTop: 40,
    marginBottom: 20,
  }
});
