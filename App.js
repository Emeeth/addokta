'use strict';
import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import PreloaderScreen from './src/screens/Preloader';
import LoginScreen from './src/screens/Login';
import UserScreen from './src/screens/User';

export default class App extends Component {
  render() {
    return (
      <Router>
        <Scene key='root' hideNavBar initial>
          <Scene key='userModule' hideNavBar>
            <Scene key='user' component={UserScreen} />
          </Scene>
          <Scene key='loginModule' hideNavBar >
            <Scene key='login' component={LoginScreen} />
          </Scene>
          <Scene key='preloaderModule' hideNavBar initial>
            <Scene key='preloader' component={PreloaderScreen} />
          </Scene>
        </Scene>
      </Router>
    );
  }
}
